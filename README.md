# Sample voting app based on files available on Docker Hub -> Docker Samples

Combine files first using:

> docker-compose -f voting_app_sample.yml -f voting_app_sample.prod.yml config > config.yml

The YML file available is this repository can be deployed using 

> docker stack deploy -c config.yml voting_app

The app should we available at:

> Voting: localhost:5000
> Resuklts: localhost:5001

The architecture of the app is shown below

![Architecture](https://github.com/BretFisher/example-voting-app/raw/master/architecture.png)